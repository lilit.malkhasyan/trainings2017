#include <iostream>

int
main()
{
    int numberOne, numberTwo;

    std::cout << "Enter two number: ";
    std::cin  >> numberOne >> numberTwo;

    if (0 == numberOne) {
        std::cout << "Error 1: Division by zero\nThe number can't be divided by 0." << std::endl;
        return 1;
    }

    if (numberTwo % numberOne == 0){
        std::cout << numberTwo << " is a multiple of " << numberOne << std::endl;
        return 0;
    }
    std::cout << numberTwo << "is not a multiple of" << numberOne << std::endl;

    return 0;
}

