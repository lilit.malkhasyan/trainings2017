#include <iostream>
#include <iomanip>

int
main()
{
    double miles = 0;
    double totalGallons = 0;
    double totalMiles = 0;
    
    while (miles != -1) {
        std::cout << "\nEnter the miles used (-1 to quit): ";
        std::cin  >> miles;
        if (-1 == miles) {
            return 0;
        }
        if (miles < 0) {
            std::cerr << "Error 1: Miles cannot be negative" << std::endl;
            return 1;
        }

        double gallons;
        std::cout << "Enter gallons: ";
        std::cin  >> gallons;
        if (gallons <= 0) {
            std::cerr << "Error 2: Gallons cannot be negative" << std::endl;
            return 2;
        }
        
        double mpg = miles / gallons;
        std::cout << "MPG this tankful: " 
                  << std::setprecision(6) 
                  << std::fixed 
                  << mpg << std::endl;
        
        totalMiles += miles;
        totalGallons += gallons;

        double totalMpg = totalMiles / totalGallons;
        std::cout << "Total MPG: " 
                  << std:: setprecision(6) 
                  << std::fixed 
                  << totalMpg << std::endl;
    }
    return 0;
}

