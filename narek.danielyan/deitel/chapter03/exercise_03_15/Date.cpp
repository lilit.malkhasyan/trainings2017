#include "Date.hpp"
#include <iostream>

Date::Date(int month, int day, int year)
{
    setMonth(month);
    setDay(day);
    setYear(year);
}

void
Date::setMonth(int month)
{
    if (month < 1) {
        month_ = 1;
        return;
    }
    if (month > 12) {
        month_ = 1;
        return;
    }
    month_ = month;
}

void
Date::setDay(int day)
{
    day_ = day;
}

void
Date::setYear(int year)
{
    year_ = year;
}

int
Date::getMonth()
{
    return month_;

}

int
Date::getDay()
{
    return day_;
}

int
Date::getYear()
{
    return year_;
}

void
Date::displayDate()
{
    std::cout << "Date in this form month/day/year: " << getMonth() <<
 "/" << getDay() << "/" << getYear() << std::endl;
}

