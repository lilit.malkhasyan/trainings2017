#include <iostream>

class Account
{
public:
    Account(int balance);
    void setBalance(int balance);
    int getBalance();
    void credit(int creditSum);
    void debit(int debitSum);

private:
    int balance_;
};

